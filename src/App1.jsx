import React from "react";
import Info from "./roughComponent/Info";
import College from "./roughComponent/College";
import TernaryOperator from "./roughComponent/TernaryOperator";
import BehaviourOfData from "./roughComponent/BehaviourOfData";
import ButtonClick from "./roughComponent/ButtonClick";
import LearnUseState1 from "./roughComponent/learnUseState/LearnUseState1";
import LearnUseState2 from "./roughComponent/learnUseState/LearnUseState2";
import ShowAndHilde1 from "./roughComponent/ShowAndHilde1";
import ShowAnd1 from "./roughComponent/ShowAnd1";

const App1 = () => {
  return (
    <div>
      {/* <Info name="samit" address="bhaktapur" age={30} isMarried={true}></Info> */}
      {/* <College name="Khwopa" address="bhaktapur" affiliation="TU"></College> */}
      {/* <TernaryOperator></TernaryOperator> */}
      {/* <BehaviourOfData
      name="nitan"
      age={30}
      isMarried={true}
      favFood={["chicken","mutton"]}
      fatherInfo={{name:"shiva",age:65}}
      myTag={<div>I am div</div>}
      ></BehaviourOfData> */}

      {/* <ButtonClick></ButtonClick> */}
      {/* <LearnUseState1></LearnUseState1> */}
      {/* <LearnUseState2></LearnUseState2> */}
      <ShowAndHilde1></ShowAndHilde1><br /><br /><br />
      <ShowAnd1></ShowAnd1>
    </div>
  );
};

export default App1;
