import React, { useState } from "react";

const ShowAnd1 = () => {
  let [show, setShow] = useState(true);
  let handle = (check) => {
    return () => {
      setShow(check);
    };
  };
  return (
    <div>
      {show ? (
        <p>
          Samit <br />
          <button onClick={handle(false)}>Hide</button>
        </p>
      ) : (
        <button onClick={handle(true)}>Show</button>
      )}
    </div>
  );
};

export default ShowAnd1;
