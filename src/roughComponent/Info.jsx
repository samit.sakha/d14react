const Info = ({ name, address, age, isMarried }) => {
  return (
    <div>
      <p>My name is {name} </p>
      <p>I live in {address}</p>
      <p>I am {age}</p>
      <p>isMarried: {isMarried.toString()}</p>
    </div>
  );
};

export default Info;
