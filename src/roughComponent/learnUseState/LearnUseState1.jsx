import React, { useState } from "react";

const LearnUseState1 = () => {
  // defining variable in React using useState

  let [name, setName] = useState("samit");
  const handleClick = (e) => {
    setName("ram");
  };

  return (
    <div>
      name is {name}
      <button onClick={handleClick}>Change Name</button>
    </div>
  );
};

export default LearnUseState1;
