import React, { useState } from "react";

const LearnUseState2 = () => {
  let [count, setCount] = useState(0);

  const handleIncrement = (e) => {
    setCount(count + 1);
  };

  const handleDecrement = (e) => {
    setCount(count - 1);
  };

  const handleReset = (e) => {
    setCount((count = 0));
  };

  return (
    <div>
      <p>count is {count}</p>
      <button onClick={handleIncrement}>Increment</button><br /><br />
      <button onClick={handleDecrement}>Decrement</button><br /><br />
      <button onClick={handleReset}>Reset</button>
    </div>
  );
};

export default LearnUseState2;
