import React, { useState } from "react";

const ShowAndHilde1 = () => {
  let [show, setShow] = useState(true);
  let handleClick = (check) => {
    return () => {
      setShow(check);
    };
  };
  return (
    <div>
      {show ? <p>Samit</p> : null}
      <button onClick={handleClick(true)}>Show</button>
      <button onClick={handleClick(false)}>Hide</button>
    </div>
  );
};

export default ShowAndHilde1;
